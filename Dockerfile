FROM alpine:3.5
MAINTAINER Milosz Tanski <milosz@adfin.com>

# Prepare the image (curl & ssl certs)
RUN apk --update upgrade && \
    apk add curl ca-certificates && \
    update-ca-certificates && \
    rm -rf /var/cache/apk/*

ENV OAUTH2_PROXY_VERSION 2.2.0
ENV OAUTH2_PROXY_FILE v2.2/oauth2_proxy-2.2.0.linux-amd64.go1.8.1.tar.gz

RUN \
  mkdir downloaded \
  && curl -sL "https://github.com/bitly/oauth2_proxy/releases/download/${OAUTH2_PROXY_FILE}" \
       | tar -zxv -f- -C downloaded \
  && find downloaded -type f -name "oauth2_proxy" -exec mv {} /bin/ \; \
  && chmod +x /bin/oauth2_proxy \
  && rm -r downloaded

ENTRYPOINT ["oauth2_proxy"]
